SELECT * FROM departments;

SELECT DISTINCT title FROM titles;

SELECT * FROM employees WHERE hire_date > '1999-12-22';

SELECT e.first_name, e.last_name, d.dept_name FROM employees e
  INNER JOIN  dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  ORDER BY d.dept_name, e.first_name;

SELECT e.first_name, e.last_name, s.salary, d.dept_name FROM employees e
  INNER JOIN salaries s on e.emp_no = s.emp_no
  INNER JOIN dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  WHERE salary > 150000
  ORDER BY e.last_name;


SELECT e.first_name, e.last_name, s.salary, d.dept_name FROM employees e
  INNER JOIN salaries s on e.emp_no = s.emp_no
  INNER JOIN dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  WHERE salary > 130000 AND s.to_date LIKE '9999%'
  ORDER BY d.dept_name;

SELECT  DISTINCT e.first_name, e.last_name, e.birth_date FROM employees e
           ORDER BY e.birth_date
           LIMIT 30;

SELECT DISTINCT e.first_name FROM employees e
  WHERE e.gender='m'
  ORDER BY e.first_name DESC;

SELECT e.first_name, e.last_name, d.dept_name FROM employees e
  INNER JOIN dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  ORDER BY e.last_name, d.dept_name;

3A

SELECT e.first_name, e.last_name, d.dept_name FROM employees e
  INNER JOIN dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  WHERE d.dept_name = 'Sales'
  LIMIT 30 OFFSET 9;

3B

SELECT e.first_name, e.last_name, d.dept_name, s.salary FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE d.dept_name = 'Sales' AND s.salary BETWEEN 100000 AND 120000
LIMIT 30;

3C
SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), e.birth_date)/360.25 AS 'Age' FROM employees e
WHERE DATEDIFF(CURDATE(), e.birth_date)/360 > 50
ORDER BY DATEDIFF(CURDATE(), e.birth_date);

SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), e.birth_date)/360.25 AS AGE FROM employees e
WHERE DATEDIFF(CURDATE(), e.birth_date)/360.25 BETWEEN 50 AND 60;

3D
SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), e.birth_date)/360.25 FROM employees e
ORDER BY DATEDIFF(CURDATE(), e.birth_date) DESC
LIMIT 30;

4A
SELECT e.first_name, e.last_name, d.dept_name FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no

4B
SELECT e.first_name, e.last_name, s.salary, t.title FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN salaries s on e.emp_no = s.emp_no
INNER JOIN titles t on e.emp_no = t.emp_no
WHERE e.first_name = 'Ebru' AND e.last_name = 'Alpin' AND s.to_date LIKE '9999%' AND t.to_date LIKE '9999%';

4C
SELECT hpe.first_name, hpe.last_name, hpe.salary, e.first_name, e.last_name
FROM (
   SELECT e.first_name, e.last_name, s.salary, dm.emp_no FROM employees e
   INNER JOIN dept_emp de on e.emp_no = de.emp_no
   INNER JOIN salaries s on e.emp_no = s.emp_no
   INNER JOIN departments d on de.dept_no = d.dept_no
   INNER JOIN dept_manager dm on d.dept_no = dm.dept_no
   WHERE de.to_date LIKE '9999%' AND s.to_date LIKE '9999%' AND dm.to_date LIKE '9999%'
   ORDER BY s.salary DESC
   LIMIT 20
   ) AS hpe
INNER JOIN employees e on hpe.emp_no = e.emp_no
ORDER BY hpe.emp_no;



5A
SELECT e.first_name, e.last_name, t.title FROM employees e
INNER JOIN titles t on e.emp_no = t.emp_no
WHERE t.to_date LIKE '9999%' AND e.first_name = 'Aamer'
ORDER BY e.first_name, t.title;

5B
SELECT e.first_name, e.last_name, s.salary FROM employees e
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE s.to_date LIKE '9999%' AND e.first_name = 'Aamer'
ORDER BY s.salary;

5C
SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), de.from_date)/365.25 AS time_in_service, d.dept_name FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
WHERE de.to_date = '9999-01-01' AND d.dept_name = 'Sales'
ORDER BY de.from_date, time_in_service;

5D
SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), e.birth_date)/365.25 AS AGE FROM employees e
ORDER BY AGE DESC
LIMIT 10;

6A
SELECT e.first_name, e.last_name, DATEDIFF(CURDATE(), de.from_date)/365.25 AS time_in_service, d.dept_name FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
WHERE de.emp_no = dm.emp_no AND de.to_date = '9999-01-01'
ORDER BY time_in_service DESC;

6B
SELECT e.first_name, e.last_name, e.birth_date, DATEDIFF(CURDATE(), de.from_date) AS time_in_service FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
WHERE de.to_date = '9999-01-01'  AND e.birth_date LIKE '%%%%-06-%%' AND DATEDIFF(CURDATE(), de.from_date) > 1000
ORDER BY time_in_service DESC;

6C
SELECT e.first_name, e.last_name, d.dept_name, e.gender
FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
WHERE e.gender = 'F';

6D

SELECT e.first_name, e.last_name, s.salary AS max_salary
FROM employees e
INNER JOIN dept_emp de on e.emp_no = de.emp_no
INNER JOIN departments d on de.dept_no = d.dept_no
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE d.dept_name = 'Sales' and de.to_date = '9999-01-01'
ORDER BY s.salary DESC
LIMIT 1;

7A

SELECT depm.first_name, depm.last_name, e.first_name, e.last_name
FROM employees e
INNER JOIN (
  SELECT e.first_name, e.last_name, e.emp_no, d.dept_no FROM employees e
  INNER JOIN dept_emp de on e.emp_no = de.emp_no
  INNER JOIN departments d on de.dept_no = d.dept_no
  INNER JOIN dept_manager dm on e.emp_no = dm.emp_no
  WHERE dm.to_date LIKE '9999%'
  ) depm
INNER JOIN dept_emp de2 on e.emp_no = de2.emp_no
WHERE depm.dept_no = de2.dept_no AND de2.to_date LIKE '9999%' AND DATEDIFF('2002-08-01', de2.from_date)<365

7B

SELECT e.first_name, e.last_name, s.salary, DATEDIFF('2002-08-01', S.from_date)
FROM employees e
INNER JOIN salaries s on e.emp_no = s.emp_no
WHERE DATEDIFF('2002-08-01', S.from_date)<365
ORDER BY DATEDIFF('2002-08-01', S.from_date) DESC

7C


SELECT count(de.dept_no) as ct, de.dept_no, d.dept_name FROM departments d
INNER JOIN dept_emp de on d.dept_no = de.dept_no
GROUP BY de.dept_no
ORDER BY ct DESC
LIMIT 1













