package com.madhatter.mysqlemployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlemployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqlemployeeApplication.class, args);
    }

}
